package py.com.jtc.rest.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import py.com.jtc.rest.client.bean.Cuenta;
import py.com.jtc.rest.client.bean.Transferencias;
import py.com.jtc.rest.client.service.CuentaService;

@Controller
@RequestMapping("/rest-client")
public class CuentasController {
	
	@Autowired
	private CuentaService cuentaService;
	
	@GetMapping("/cuentas/{idusuario}")
	public String listaCuenta(@PathVariable("idusuario") Integer idusuario, Model model) { 

		model.addAttribute("cuenta", cuentaService.obtenerCtaUsuario(idusuario));
		return "cuentas";
	}
	
	@GetMapping("/cuentas/transferencias/{idcuenta}")
	public String ctaTransferencia(@PathVariable("idcuenta") Integer idcuenta, Model model) { 
		System.out.println("---");
		Cuenta cuenta = cuentaService.obtenerCuenta(idcuenta);
		Transferencias t = new Transferencias();
		t.setCuentaDebito(cuenta.getNroCuenta());
		t.setIdCuenta(cuenta.getIdCuenta());
		t.setMoneda(cuenta.getMoneda());
		t.setSaldo(cuenta.getSaldo());
		System.out.println("--");
		model.addAttribute("transferencias", t);
		return "transferencias";
	}
	
	@PostMapping("/cuentas/ctatransferencia")
	public String ctaTransferencia(@ModelAttribute("transferencias") Transferencias transferencia) {
		System.out.println("aca si que se pudre1");
		cuentaService.ctaTransferencia(transferencia);
		Transferencias t = new Transferencias();
		t.setCuentaDebito(transferencia.getCuentaDebito());
		t.setCuentaCredito(transferencia.getCuentaCredito());
		t.setMonto(transferencia.getMonto());
		t.setMoneda(transferencia.getMoneda());
		System.out.println("aca si que se pudre2");
		return "movimientos";
		//return "redirect:/rest-cuenta/cuenta";
	}
	
	@GetMapping("/cta/transferencias/{nrodocumento}")
	public String ctransferencia(@PathVariable("nrodocumento") Integer nrodocumento, Model model) { 
		
		//Cuenta cuenta = cuentaService.obtenerCuentaDoc(nrodocumento);
		//Transferencias t = new Transferencias();
		//t.setCuentaDebito(cuenta.getNroCuenta());
		//model.addAttribute("transferencias", t);
		return "transferencias";
	}

	@GetMapping("/cuentas")
	public String listaCuentas(Model model) { 
		System.out.println("cuentas");
		model.addAttribute("cuentas", cuentaService.obtenerCuentas());
		return "cuentas";
	}
	
	@GetMapping("/cuentas/add")
	public String cuentasForm(Model model) {
		model.addAttribute("cuenta", new Cuenta());
		return "cuentas";
	}
	
	@GetMapping("/cuentas/edit/{id}")
	public String cuentasEdit(@PathVariable("id") Integer id, Model model) {
		model.addAttribute("cuenta", cuentaService.obtenerCuenta(id));
		return "cuentas";
	}
	
	@PostMapping("/cuentas")
	public String agregarCuenta(@ModelAttribute("cuenta") Cuenta cuenta) {
		cuentaService.insertarCuenta(cuenta);
		if (cuenta == null) {
			
		}
		
		return "redirect:/rest-client/cuentas";
	}
	
	@GetMapping("/cuentas/delete/{id}")
	public String deleteCuenta(@PathVariable("id") int id) {
		cuentaService.eliminarCuenta(id);
		return "redirect:/rest-client/cuentas";
	}

}
