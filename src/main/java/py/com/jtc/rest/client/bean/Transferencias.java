package py.com.jtc.rest.client.bean;

public class Transferencias {
	private Integer idCuenta;
	private String cuentaCredito;
	private String cuentaDebito;
	private String tipo;
	private String moneda;
	private Integer monto;
	private Integer saldo;
	
	public Integer getIdCuenta() {
		return idCuenta;
	}
	
	public void setIdCuenta(Integer idCuenta) {
		this.idCuenta = idCuenta;
	}
	
	public String getCuentaCredito() {
		return cuentaCredito;
	}
	
	public void setCuentaCredito(String cuentaCredito) {
		this.cuentaCredito = cuentaCredito;
	}
	
	public String getCuentaDebito() {
		return cuentaDebito;
	}
	
	public void setCuentaDebito(String cuentaDebito) {
		this.cuentaDebito = cuentaDebito;
	}
	
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getMoneda() {
		return moneda;
	}
	
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	
	public Integer getSaldo() {
		return saldo;
	}
	
	public void setSaldo(Integer saldo) {
		this.saldo = saldo;
	}
	
	public Integer getMonto() {
		return monto;
	}
	
	public void setMonto(Integer monto) {
		this.monto = monto;
	}
	
	
	
}