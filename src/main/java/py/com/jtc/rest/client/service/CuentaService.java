package py.com.jtc.rest.client.service;

import java.util.List;

import py.com.jtc.rest.client.bean.Cuenta;
import py.com.jtc.rest.client.bean.Transferencias;

public interface CuentaService {
	
	List<Cuenta> obtenerCuentas();
	
	Cuenta obtenerCuenta(Integer idcuenta);
	
	List<Cuenta> obtenerCtaUsuario(Integer idusuario);
	
	void insertarCuenta(Cuenta cuenta);

	Cuenta editarCuenta(Integer id, Cuenta cuenta);
	
	void ctaTransferencia(Transferencias transferencia);
	
	void eliminarCuenta(Integer id);
}