package py.com.jtc.rest.client.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import py.com.jtc.rest.client.bean.Cuenta;
import py.com.jtc.rest.client.bean.Transferencias;

@Service
public class CuentaServiceImpl  implements CuentaService{
	private static List<Cuenta> cuentas;
	public static final String URL_API = "http://localhost:9090/rest/cuentas/";
	
	@Override
	public List<Cuenta> obtenerCuentas() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Cuenta[]> responseEntity = restTemplate.getForEntity(URL_API, Cuenta[].class);
		
		cuentas = Arrays.asList(responseEntity.getBody());
		return cuentas;
	}
	
	@Override
	public List<Cuenta> obtenerCtaUsuario(Integer idusuario) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("idusuario",idusuario);
	    
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Cuenta[]> responseEntity = restTemplate.getForEntity(URL_API+"{idusuario}", Cuenta[].class, params);                                                  
		cuentas = Arrays.asList(responseEntity.getBody());

		return cuentas;
	}
	
	@Override
	public Cuenta obtenerCuenta(Integer idcuenta) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("idcuenta",idcuenta);
	    
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Cuenta> responseEntity = restTemplate.getForEntity(URL_API+"transferencias/{idcuenta}", Cuenta.class, params);

		Cuenta cuenta = responseEntity.getBody();
		return cuenta;
	}
	
	@Override
	public void ctaTransferencia(Transferencias transferencia) {
		RestTemplate restTemplate = new RestTemplate();
		System.out.println("aca si que se pudre.1");
		restTemplate.postForEntity(URL_API+"ctaTransferencia", transferencia, Transferencias.class);
		
	}

	@Override
	public void insertarCuenta(Cuenta cuenta) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Cuenta> responseEntity = restTemplate.postForEntity(URL_API, cuenta, Cuenta.class);
		
	}
	
	@Override
	public Cuenta editarCuenta(Integer id, Cuenta cuenta) {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.put(URL_API+"{id}", cuenta, id);

		return cuenta;
	}
	
	@Override
	public void eliminarCuenta(Integer id) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("id",id);
	    RestTemplate restTemplate = new RestTemplate();
	    restTemplate.delete(URL_API + "{id}",params);
	    
	}

}