package py.com.jtc.rest.client.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/home")
public class HomeController {

	@GetMapping
	public String menu(Model model) {
		String nrodocumento = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("nrodocumento", nrodocumento);
		return "home";
	}
	
}